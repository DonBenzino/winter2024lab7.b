public class Card{
	private Suit suit;
	private Rank val;
	//private boolean used;
	
	public Card(Suit suit, Rank val){
		this.suit = suit;
		this.val = val;
		//this.used = false;
	}
	public Suit getSuit(){
		return this.suit;
	}
	public Rank getVal(){
		return this.val;
	}
	/*
	public boolean getStatus(){
		return this.used;
	}
	
	public void setStatus(boolean change){
		this.used = change;
	}*/
	public double calculateScore(){
		double result = this.val.getScore() + this.suit.getScore();
		return result;
	}
	
	public String toString(){
		return this.val + " of " + this.suit;
	}
}