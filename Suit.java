public enum Suit{
	HEARTS(0.4),
	DIAMONDS(0.3),
	SPADES(0.2),
	CLUBS(0.1);
	
	private final double score;
	
	private Suit(double score){
		this.score = score;
	}
	
	public double getScore(){
		return this.score;
	}
}
	